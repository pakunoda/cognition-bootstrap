package sandbox

import com.typesafe.scalalogging.slf4j._
import org.cognition.servlet._
import org.cognition.handlers.ServletHandler
import controllers._
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.ServletConfig
import javax.servlet.annotation.WebServlet

@WebServlet(name = "sandbox", urlPatterns = Array("/*"), asyncSupported = true)
class MainServlet extends ServletHandler with LazyLogging {

  override def init(config: ServletConfig) {
    logger.info("starting...")
    RecallServlet.allControllers.map(register)
    logger.info("finished loading")
    super.init(config)
  }

}

object RecallServlet {

  val allControllers = Vector(
    new HomeController
  )

}
