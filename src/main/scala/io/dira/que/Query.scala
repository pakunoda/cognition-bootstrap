package io.dira.que

import java.sql.{ Connection, PreparedStatement, Date, Timestamp, Time, Types, Statement, ResultSet }
import scala.collection.mutable.{ ArrayBuffer, HashMap, StringBuilder }

object Query {

  def apply(query: String)(implicit connection: Connection) = new Query(query, connection)

}

class Query(query: String, connection: Connection) {

  private[this] var parameters: Seq[Any] = null

  def params(args: Any*) = {
    parameters = args
    this
  }

  def on(args: (String, Any)*) = {
    ???
  }

  def update(): Int = {
    var statement: PreparedStatement = null
    try {
      statement = connection.prepareStatement(query)
      addParameters(statement)
      statement.executeUpdate()
    }
    finally {
      statement.close()
    }
  }

  def count(): Long = select(_.getLong(1)).headOption.getOrElse(0)

  def exec() {
    var statement: PreparedStatement = null
    try {
      statement = connection.prepareStatement(query)
      addParameters(statement)
      statement.execute()
    }
    finally {
      statement.close()
    }
  }

  def select[T](f: ResultSet => T): Seq[T] = {
    var rs: ResultSet = null
    var statement: PreparedStatement = null
    try {
      statement = connection.prepareStatement(query)
      addParameters(statement)
      rs = statement.executeQuery()
      val result = new ArrayBuffer[T]
      while (rs.next())
        result += f(rs)
      result
    }
    finally {
      if (rs != null) rs.close()
      if (statement != null) statement.close()
    }
  }
  
  def addParameters(statement: PreparedStatement) {
    if (parameters != null) {
      var count = 0
      val vector = parameters.toVector
      var targetSize = vector.size

      while (count < targetSize) {
        addParameter(statement, vector(count), count + 1)
        count = count + 1
      }
    }
  }

  private[this] def addParameter[T](statement: PreparedStatement, parameter: T, location: Int) {
    parameter match {
      case e: Int =>
        statement.setInt(location, e)
      case e: String =>
        statement.setString(location, e)
      case e: Long =>
        statement.setLong(location, e)
      case e: Timestamp =>
        statement.setTimestamp(location, e)
      case e: Time =>
        statement.setTime(location, e)
      case e: Date =>
        statement.setDate(location, e)
      case e: Float =>
        statement.setFloat(location, e)
      case e: Double =>
        statement.setDouble(location, e)
      case e: Short =>
        statement.setShort(location, e)
      case e: Option[_] => e match {
        case Some(f) => addParameter(statement, f, location)
        case None => statement.setObject(location, Types.NULL)
      }
      case e: Array[Byte] =>
        statement.setBytes(location, e)
      case e =>
        if (e == null)
          statement.setObject(location, Types.NULL)
        else
          statement.setObject(location, e)
    }
  }


}
