package io.dira.que

import java.sql.{ Connection, PreparedStatement, Date, Timestamp, Time, Types, Statement, ResultSet }
import scala.collection.mutable.ArrayBuffer

object Parser {

  def apply[A, B](f: Function[A, B])(rs: ResultSet): B = f(read(1, rs))
  def apply[A, B, C](f: Function2[A, B, C])(rs: ResultSet): C =
    f(
      read(1, rs),
      read(2, rs)
    )
  def apply[A, B, C, D](f: Function3[A, B, C, D])(rs: ResultSet): D =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs)
    )
  def apply[A, B, C, D, E](f: Function4[A, B, C, D, E])(rs: ResultSet): E =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs)
    )
  def apply[A, B, C, D, E, F](f: Function5[A, B, C, D, E, F])(rs: ResultSet): F =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs)
    )
  def apply[A, B, C, D, E, F, G](f: Function6[A, B, C, D, E, F, G])(rs: ResultSet): G =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs)
    )
  def apply[A, B, C, D, E, F, G, H](f: Function7[A, B, C, D, E, F, G, H])(rs: ResultSet): H =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I](f: Function8[A, B, C, D, E, F, G, H, I])(rs: ResultSet): I =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J](f: Function9[A, B, C, D, E, F, G, H, I, J])(rs: ResultSet): J =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K](f: Function10[A, B, C, D, E, F, G, H, I, J, K])(rs: ResultSet): K =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K, L](f: Function11[A, B, C, D, E, F, G, H, I, J, K, L])(rs: ResultSet): L =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs),
      read(11, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K, L, M](f: Function12[A, B, C, D, E, F, G, H, I, J, K, L, M])(rs: ResultSet): M =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs),
      read(11, rs),
      read(12, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K, L, M, N](f: Function13[A, B, C, D, E, F, G, H, I, J, K, L, M, N])(rs: ResultSet): N =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs),
      read(11, rs),
      read(12, rs),
      read(13, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K, L, M, O, P](f: Function14[A, B, C, D, E, F, G, H, I, J, K, L, M, O, P])(rs: ResultSet): P =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs),
      read(11, rs),
      read(12, rs),
      read(13, rs),
      read(14, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K, L, M, O, P, Q](f: Function15[A, B, C, D, E, F, G, H, I, J, K, L, M, O, P, Q])(rs: ResultSet): Q =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs),
      read(11, rs),
      read(12, rs),
      read(13, rs),
      read(14, rs),
      read(15, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K, L, M, O, P, Q, R](f: Function16[A, B, C, D, E, F, G, H, I, J, K, L, M, O, P, Q, R])(rs: ResultSet): R =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs),
      read(11, rs),
      read(12, rs),
      read(13, rs),
      read(14, rs),
      read(15, rs),
      read(16, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K, L, M, O, P, Q, R, S](f: Function17[A, B, C, D, E, F, G, H, I, J, K, L, M, O, P, Q, R, S])(rs: ResultSet): S =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs),
      read(11, rs),
      read(12, rs),
      read(13, rs),
      read(14, rs),
      read(15, rs),
      read(16, rs),
      read(17, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T](f: Function18[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, T])(rs: ResultSet): T =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs),
      read(11, rs),
      read(12, rs),
      read(13, rs),
      read(14, rs),
      read(15, rs),
      read(16, rs),
      read(17, rs),
      read(18, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U](f: Function19[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, T, U])(rs: ResultSet): U =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs),
      read(11, rs),
      read(12, rs),
      read(13, rs),
      read(14, rs),
      read(15, rs),
      read(16, rs),
      read(17, rs),
      read(18, rs),
      read(19, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V](f: Function20[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, T, U, V])(rs: ResultSet): V =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs),
      read(11, rs),
      read(12, rs),
      read(13, rs),
      read(14, rs),
      read(15, rs),
      read(16, rs),
      read(17, rs),
      read(18, rs),
      read(19, rs),
      read(20, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W](f: Function21[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, T, U, V, W])(rs: ResultSet): W =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs),
      read(11, rs),
      read(12, rs),
      read(13, rs),
      read(14, rs),
      read(15, rs),
      read(16, rs),
      read(17, rs),
      read(18, rs),
      read(19, rs),
      read(20, rs),
      read(21, rs)
    )
  def apply[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X](f: Function22[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, T, U, V, W, X])(rs: ResultSet): X =
    f(
      read(1, rs),
      read(2, rs),
      read(3, rs),
      read(4, rs),
      read(5, rs),
      read(6, rs),
      read(7, rs),
      read(8, rs),
      read(9, rs),
      read(10, rs),
      read(11, rs),
      read(12, rs),
      read(13, rs),
      read(14, rs),
      read(15, rs),
      read(16, rs),
      read(17, rs),
      read(18, rs),
      read(19, rs),
      read(20, rs),
      read(21, rs),
      read(22, rs)
    )

}

