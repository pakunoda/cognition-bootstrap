package io.dira

import java.sql.ResultSet
import scala.util.Try

package object que {

  def read[T](location: Int, rs: ResultSet): T = getObject[T](rs.getObject(location))
  def read[T](columnName: String, rs: ResultSet): T = getObject[T](rs.getObject(columnName))

  private[this] def getObject[T](obj: Object): T = obj.asInstanceOf[T]

}
