package controllers

import org.cognition._

trait Secured extends Security[String] {
  
  override def token()(implicit request: Request): Option[String] = {
    None
  }

}
