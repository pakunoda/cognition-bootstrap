package controllers

import org.cognition.servlet._

class HomeController extends ServletController {

  get("/") { implicit request =>
    render.html(html.index())
  }

}
