package testing

import scala.collection.concurrent.TrieMap
import org.openqa.selenium.phantomjs._
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.{Capabilities, WebDriver, WebElement, By}

object SpecHelper {

  val drivers = TrieMap[String, WebDriver]()

  def createDriver: WebDriver = new PhantomJSDriver(createCap)

  private def createCap = {
    val caps = DesiredCapabilities.phantomjs()
    caps.setCapability("takesScreenshot", true)
    caps.setCapability(
      PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
      "/usr/local/Cellar/phantomjs/1.9.1/bin/phantomjs"
      )
    caps
  }

}
