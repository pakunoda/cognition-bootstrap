package testing

import org.scalatest._
import org.scalatest.matchers._
import org.scalatest.prop.Checkers

import com.typesafe.config._

import org.openqa.selenium.phantomjs._
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.{Capabilities, WebDriver, WebElement, By}

import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito._
import org.mockito.Mockito.{ when => mwhen, _ }
import org.mockito.Matchers.{eq => the, any}
import SpecHelper._

trait BaseSpec extends FeatureSpec with GivenWhenThen with ShouldMatchers with MockitoSugar with Checkers {
}
