resolvers += Resolver.url("pakunoda alphas", url("https://raw.github.com/pakunoda/mvn-repo/alphas"))(Resolver.ivyStylePatterns)

resolvers += "spray repo" at "http://repo.spray.io"

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "0.6.4")

addSbtPlugin("io.spray" % "sbt-revolver" % "0.7.1")

addSbtPlugin("io.spray" % "sbt-twirl" % "0.7.0")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.11.2")

addSbtPlugin("com.earldouglas" % "xsbt-web-plugin" % "0.9.0")
