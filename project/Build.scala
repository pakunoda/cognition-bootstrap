import sbt._
import Keys._
import twirl.sbt.TwirlPlugin._
import sbtassembly.Plugin._
import AssemblyKeys._
import spray.revolver.RevolverPlugin._
import com.typesafe.sbt.SbtNativePackager._

object Bench extends Build {
  def initSqltyped {
    System.setProperty("sqltyped.url", "jdbc:postgresql://127.0.0.1/thoughts")
    System.setProperty("sqltyped.driver", "org.postgresql.Driver")
    System.setProperty("sqltyped.username", "testing")
    System.setProperty("sqltyped.password", "testing")
    System.setProperty("sqltyped.schema", "public")
  }

  lazy val project = Project(
    "Project", 
    file("."),
    settings = Defaults.defaultSettings ++ assemblySettings ++ com.earldouglas.xsbtwebplugin.WebPlugin.webSettings ++ Twirl.settings ++ Seq(
      scalaVersion := "2.11.1",
      version := "1.0.0",
      name := "sandbox",
      libraryDependencies ++= Seq(
        // Core
        "org.cognition" %% "cognition" % "0.9.0",
        "joda-time" % "joda-time" % "2.3",
        "org.eclipse.jetty" % "jetty-webapp" % "9.1.3.v20140225" % "container",                                                                                    
        "org.eclipse.jetty" % "jetty-plus" % "9.1.3.v20140225" % "container",                                                                                      
        "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
        // Logging
        "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2",
        "ch.qos.logback" % "logback-classic" % "1.1.2",
        "ch.qos.logback" % "logback-core" % "1.1.2",
        "log4j" % "log4j" % "1.2.17",
        // Database
        "com.typesafe.slick" % "slick_2.11.0-RC4" % "2.1.0-M1",
        "com.jolbox" % "bonecp" % "0.8.0.RELEASE",
        "org.postgresql" % "postgresql" % "9.3-1100-jdbc41",
        // Testing
        "org.scalatest" %% "scalatest" % "2.2.0-M1" % "test",
        "org.scalacheck" %% "scalacheck" % "1.11.4" % "test",
        "org.mockito" % "mockito-core" % "1.9.5" % "test",
        "com.github.javafaker" % "javafaker" % "0.3" % "test"
      ),
      initialize ~= { _ => initSqltyped },
      resolvers ++= Seq(
        "cognition" at "https://raw.github.com/pakunoda/mvn-repo/master",
        "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
      ),
      mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
      {
        case s if s.endsWith(".class") => MergeStrategy.first
        case s if s.endsWith("project.clj") => MergeStrategy.discard
        case s if s.endsWith("pom.properties") => MergeStrategy.discard
        case s if s.endsWith("pom.xml") => MergeStrategy.discard
        case s if s.endsWith(".html") => MergeStrategy.last
        case s if s.endsWith(".dtd") => MergeStrategy.last
        case s if s.endsWith(".xsd") => MergeStrategy.last
        case s if s.endsWith(".jnilib") => MergeStrategy.rename
        case s if s.endsWith("jansi.dll") => MergeStrategy.rename
        case s if s.endsWith("LICENSE.txt") => MergeStrategy.first
        case PathList("META-INF", xs @ _*) => MergeStrategy.discard
        case PathList("rootdoc.txt", xs @ _*) => MergeStrategy.discard
        case PathList("log4j.properties", xs @ _*) => MergeStrategy.first
        case x => MergeStrategy.deduplicate
        }
      },
      fork in Test := true,
      test in assembly := {},
      incOptions := incOptions.value.withNameHashing(true),
      scalacOptions ++= Seq("-feature", "-deprecation"),
      slick <<= slickCodeGenTask
      //, sourceGenerators in Compile <+= slickCodeGenTask // register automatic code generation on every compile, remove for only manual use
    )
  )

  // code generation task
  lazy val slick = TaskKey[Seq[File]]("gen-tables")
  lazy val slickCodeGenTask = (sourceManaged, dependencyClasspath in Compile, runner in Compile, streams) map { (dir, cp, r, s) =>
    val outputDir = (dir / "slick").getPath // place generated files in sbt's managed sources folder
    val url = "jdbc:postgresql://127.0.0.1/thoughts?user=testing&password=testing" // connection info for a pre-populated throw-away, in-memory db for this demo, which is freshly initialized on every run
    val jdbcDriver = "org.postgresql.Driver"
    val slickDriver = "scala.slick.driver.PostgresDriver"
    val pkg = "models"
    toError(r.run("scala.slick.model.codegen.SourceCodeGenerator", cp.files, Array(slickDriver, jdbcDriver, url, outputDir, pkg), s.log))
    val fname = outputDir + "/models/Tables.scala"
    Seq(file(fname))
  }
}
